int vSpeed = 127;        // MAX 255
int turn_speed = 230;    // MAX 255
int turn_delay = 150;
 
// Motor A connections
/*int enA = 9;
int in1 = 8;
int in2 = 7;
// Motor B connections
int enB = 3;
int in3 = 5;
int in4 = 4;*/




const int LMotor_FPIN = 21;
const int LMotor_BPIN = 20;


const int RMotor_FPIN = 8;
const int RMotor_BPIN = 7;


//Sensor Connection
const int LEYE = 10;
const int REYE = 11;

//int left_sensor_state;
//int right_sensor_state;

//const int motorBspeed = 11;
//const int motorAspeed = 10;

const int LMotor_PWM = A2;
const int RMotor_PWM = A3;

void setup() {
  Serial.begin(9600);

  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );

  //set all the motor control pins to outputs
  //pinMode(enA, OUTPUT);
  //pinMode(enB, OUTPUT);
  pinMode(LMotor_FPIN, OUTPUT);
  pinMode(LMotor_BPIN, OUTPUT);
  pinMode(RMotor_FPIN, OUTPUT);
  pinMode(RMotor_BPIN, OUTPUT);

  // Turn off motors - Initial state
  digitalWrite(LMotor_FPIN, LOW);
  digitalWrite(LMotor_BPIN, LOW);
  digitalWrite(RMotor_FPIN, LOW);
  digitalWrite(RMotor_BPIN, LOW);
 
}

void loop() {

//left_sensor_state = analogRead(LEYE);
//right_sensor_state = analogRead(REYE);

if(digitalRead( REYE ) == HIGH && digitalRead( LEYE ) == LOW)
{
  Serial.println("turning right");

  digitalWrite (LMotor_FPIN,HIGH); 
  digitalWrite (LMotor_BPIN,LOW);                      
  digitalWrite (RMotor_FPIN,HIGH);  
  digitalWrite (RMotor_BPIN,LOW);

  analogWrite (RMotor_PWM, 64);
  analogWrite (LMotor_PWM, turn_speed);

  delay(turn_delay);
 
 }
 
if(digitalRead( REYE ) == LOW && digitalRead( LEYE ) == HIGH)
{
  Serial.println("turning left");
 
  digitalWrite (LMotor_FPIN,HIGH); 
  digitalWrite (LMotor_BPIN,LOW);                      
  digitalWrite (RMotor_FPIN,HIGH);  
  digitalWrite (RMotor_BPIN,LOW);

  analogWrite (RMotor_PWM, turn_speed);
  analogWrite (LMotor_PWM, 64);

  delay(turn_delay);
 }

if(digitalRead( REYE ) == LOW && digitalRead( LEYE ) == LOW)
{
  Serial.println("going forward");

  digitalWrite (LMotor_FPIN,HIGH); 
  digitalWrite (LMotor_BPIN,LOW);                      
  digitalWrite (RMotor_FPIN,HIGH);  
  digitalWrite (RMotor_BPIN,LOW);

  analogWrite (RMotor_PWM, 160);
  analogWrite (LMotor_PWM, 170 );

  delay(turn_delay);
 
  }

if(digitalRead( REYE ) == HIGH && digitalRead( LEYE ) == HIGH)
{
  Serial.println("stop");

  digitalWrite (LMotor_FPIN,LOW); 
  digitalWrite (LMotor_BPIN,LOW);                      
  digitalWrite (RMotor_FPIN,LOW);  
  digitalWrite (RMotor_BPIN,LOW);
 
  analogWrite (0, 0);
  analogWrite (0, 0); 
}

}
