#include <WiFiNINA.h>

char ssid[] = "2E10NET";//AP network will appear like this to PC
char pass[] = "2E10Project";//required password


WiFiServer server(5200);

bool toMove = false;
//bool toUpdate = false;

const int SPEED = 255;

const int US_TRIG = 5;
const int US_ECHO = 4;
int distance;
long duration;

// Left and Right IR Sensors
const int LEYE = 11;
const int REYE = 10;

//MOTOR PWM PINS
const int LMotor_PWM = A3;
const int RMotor_PWM = A2;

// Motor A
const int RHS_Red = 21;
const int RHS_Black = 20;

// Motor B
const int LHS_Red = 8;
const int LHS_Black = 7;

void setup() {
  Serial.begin(9600);
  pinMode(US_TRIG, OUTPUT);
  pinMode(US_ECHO, INPUT);
  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );
  pinMode(LHS_Red, OUTPUT);
  pinMode(LHS_Black, OUTPUT);
  pinMode(RHS_Red, OUTPUT);
  pinMode(RHS_Black, OUTPUT);

  WiFi.beginAP(ssid, pass);
  IPAddress ip = WiFi.localIP();//Open Acces Point Network
  Serial.print("IP Address:");
  Serial.println(ip);
  server.begin();
}

void loop() {
  WiFiClient client = server.available();
  
  digitalWrite(US_TRIG, LOW);
  delayMicroseconds(2);
  digitalWrite(US_TRIG, HIGH);
  delayMicroseconds(10);
  duration = pulseIn(US_ECHO, HIGH);
  distance = (duration / 2) / 29.1;
  delay(10);

  if (client.available()) {
    char c = client.read();

    if (c == 'g') {
      toMove = true;
      //      if (toUpdate == true) {
      //        client.write(distance);
      //    }
    }
    if (c == 's') {
      toMove = false;
    }

  }

  if (toMove == true) {


    if (distance <= 19) {
      brake_wheels();
      //    toUpdate = true;
      client.write(distance);
    }

    else {

      if ((digitalRead( LEYE ) == HIGH ) && (digitalRead( REYE ) == HIGH )) {
        brake_wheels();
      }
      if ((digitalRead( LEYE ) == LOW ) && (digitalRead( REYE ) == LOW )) {
        forwards();
      }
      if ((digitalRead( LEYE ) == LOW ) && (digitalRead( REYE ) == HIGH )) {
        turn_right();
      }
      if ((digitalRead( LEYE ) == HIGH ) && (digitalRead( REYE ) == LOW )) {
        turn_left();
      }
    }


  }

  if (toMove == false) {
    brake_wheels();
  }

}

void brake_wheels() {
  digitalWrite(LHS_Red, LOW);
  digitalWrite(LHS_Black, LOW);
  digitalWrite(RHS_Red, LOW);
  digitalWrite(RHS_Black, LOW);
}

void forwards() {
  analogWrite(LMotor_PWM,255);
  analogWrite(RMotor_PWM,255);
  digitalWrite(LHS_Red, HIGH);
  digitalWrite(LHS_Black, LOW);
  digitalWrite(RHS_Red, HIGH);
  digitalWrite(RHS_Black, LOW);
}

void backwards() {
  analogWrite(LMotor_PWM,255);
  analogWrite(RMotor_PWM,255);
  digitalWrite(LHS_Red, LOW);
  digitalWrite(LHS_Black, HIGH);
  digitalWrite(RHS_Red, LOW);
  digitalWrite(RHS_Black, HIGH);
}

void turn_right() {
  analogWrite(LMotor_PWM,255);
  analogWrite(RMotor_PWM,127);
  digitalWrite(LHS_Red, HIGH);
  digitalWrite(LHS_Black, LOW);
  digitalWrite(RHS_Red, HIGH);
  digitalWrite(RHS_Black, LOW);
}

void turn_left() {
  analogWrite(LMotor_PWM,127);
  analogWrite(RMotor_PWM,255);
  digitalWrite(LHS_Red, HIGH);
  digitalWrite(LHS_Black, LOW);
  digitalWrite(RHS_Red, HIGH);
  digitalWrite(RHS_Black, LOW);
}
