import controlP5.*;
import processing.net.*;

ControlP5 cp5;
Client myClient;
int data;
boolean hasUpdated = false;
boolean toPrint = true;
char update;

void setup() {
  size(400, 300);
  cp5 = new ControlP5(this);
  myClient = new Client(this, "192.168.1.2", 5200); 

  cp5.addButton("go")
    //.setValue(0)
    .setPosition(50, 70)
    .setSize(300, 60);

  cp5.addButton("brakes")
    //.setValue(0)
    .setPosition(50, 150)
    .setSize(300, 60);
}

void draw() {
  if (myClient.available() > 0) {
    data = myClient.read();

    if (data > 0 && data < 16) {
      if (!hasUpdated && toPrint) {
        println("Stopping for an obstacle within 15 cm.");
        //print("Stopping for obstacle at ");
        //print(data);
        //println(" cm.");
        hasUpdated = true;
      }
    }
    if (hasUpdated == true) {
      toPrint = false;
    }
    if (!toPrint) {
      // check if something will change this value
      update = myClient.readChar();

      if (update == 'x') {
        toPrint = false;
      }

      if (update == 'c') {
        toPrint = true;
      }
    }
    if (toPrint) {
      hasUpdated = false;
    }
    data = myClient.read();
  }
}


public void go() {
  if (myClient.active()) {
    myClient.write("g");
    println("GO key pressed. Starting the buggy.");
  }
}

public void brakes() {
  if (myClient.active()) {
    myClient.write("s");
    println("BRAKES key pressed. Stopping the buggy.");
  }
}
