import controlP5.*;
import processing.net.*;

ControlP5 cp5;
Client myClient;
int data;
String update;

void setup() {
  size(200, 200);
  cp5 = new ControlP5(this);
  myClient = new Client(this, "192.168.1.2", 5200); 

  cp5.addButton("go")
    //.setValue(0)
    .setPosition(25, 50)
    .setSize(150, 30);

  cp5.addButton("brakes")
    //.setValue(0)
    .setPosition(25, 90)
    .setSize(150, 30);
}

void draw() {
  if (myClient.available() > 0) {
    data = myClient.read();
  
    if (data > 0 && data < 20) {
      print("Stopping for obstacle at ");
      print(data);
      println(" cm.");
    }
    
  }
 
}


public void go() {
  if (myClient.active()) {
    myClient.write("g");
    println("GO key pressed. Starting the buggy.");
  }
}

public void brakes() {
  if (myClient.active()) {
    myClient.write("s");
    println("BRAKES key pressed. Stopping the buggy.");
  }
}


