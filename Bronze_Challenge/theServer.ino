#include <WiFiNINA.h>

char ssid[] = "networkName";
char pass[] = "***********";

WiFiServer server(5200);

bool toMove = false;
//bool toUpdate = false;

const int SPEED = 255;

const int US_TRIG = 10;
const int US_ECHO = 9;
int distance;
long duration;

// Left and Right IR Sensors
const int LEYE = 3;
const int REYE = 11;

// Motor A
const int RHS_Red = 6;
const int RHS_Black = 5;

// Motor B
const int LHS_Red = 2;
const int LHS_Black = 12;

void setup() {
  Serial.begin(9600);
  pinMode(US_TRIG, OUTPUT);
  pinMode(US_ECHO, INPUT);
  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );
  pinMode(LHS_Red, OUTPUT);
  pinMode(LHS_Black, OUTPUT);
  pinMode(RHS_Red, OUTPUT);
  pinMode(RHS_Black, OUTPUT);

  WiFi.begin(ssid, pass);
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address:");
  Serial.println(ip);
  server.begin();
}

void loop() {
  WiFiClient client = server.available();
  
  digitalWrite(US_TRIG, LOW);
  delayMicroseconds(2);
  digitalWrite(US_TRIG, HIGH);
  delayMicroseconds(10);
  duration = pulseIn(US_ECHO, HIGH);
  distance = (duration / 2) / 29.1;
  delay(10);

  if (client.available()) {
    char c = client.read();

    if (c == 'g') {
      toMove = true;
      //      if (toUpdate == true) {
      //        client.write(distance);
      //    }
    }
    if (c == 's') {
      toMove = false;
    }

  }

  if (toMove == true) {


    if (distance <= 19) {
      brake_wheels();
      //    toUpdate = true;
      client.write(distance);
    }

    else {

      if ((digitalRead( LEYE ) == HIGH ) && (digitalRead( REYE ) == HIGH )) {
        brake_wheels();
      }
      if ((digitalRead( LEYE ) == LOW ) && (digitalRead( REYE ) == LOW )) {
        forwards();
      }
      if ((digitalRead( LEYE ) == LOW ) && (digitalRead( REYE ) == HIGH )) {
        turn_right();
      }
      if ((digitalRead( LEYE ) == HIGH ) && (digitalRead( REYE ) == LOW )) {
        turn_left();
      }
    }


  }

  if (toMove == false) {
    brake_wheels();
  }

}

void brake_wheels() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, 0);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, 0);
}

void forwards() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, SPEED);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, SPEED);
}

void backwards() {
  analogWrite(LHS_Red, SPEED);
  analogWrite(LHS_Black, 0);
  analogWrite(RHS_Red, SPEED);
  analogWrite(RHS_Black, 0);
}

void turn_right() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, SPEED);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, 0);
}

void turn_left() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, 0);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, SPEED);
}
