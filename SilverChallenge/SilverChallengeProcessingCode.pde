import controlP5.*;
import processing.net.*;

ControlP5 cp5;
Client myClient;
PFont f;

int data;
boolean started = false;
boolean hasUpdated = false;
boolean toPrint = true;
char update;

void setup() {
  size(640, 300);
  cp5 = new ControlP5(this);
  myClient = new Client(this, "192.168.68.125", 5200); 

  //printArray(PFont.list()); // lists out all available font
  f = createFont("HoeflerText-Regular", 24);
  textFont(f);

  fill(255);
  rect(10, 10, 620, 200, 12);

  cp5.addButton("go")
    //.setValue(0)
    .setPosition(100, 225)
    .setSize(200, 60);

  cp5.addButton("stop")
    //.setValue(0)
    .setPosition(340, 225)
    .setSize(200, 60);
}

void draw() 
{
  double previousData = 0;
  
  noStroke();
  fill(255);
  rect(120, 100, 500, 30);
  fill(0, 102, 153);
  
  while ((myClient.available() > 0) & (started == true) ) 
  {
    data = myClient.read();
    if (data != previousData)
    {
      updateText(data);
    }
    previousData = data; 
  }
}


public void go() {
  if (myClient.active()) {
    myClient.write("g");
    noStroke();
    fill(255);
    rect(100, 100, 500, 30);
    fill(0, 102, 153);
    text("GO Key pressed. Starting the buggy.", 130, 120 );
    started = true;
  }
}

public void stop() {
  if (myClient.active()) {
    myClient.write("s");
    noStroke();
    fill(255);
    rect(120, 100, 500, 30);
    fill(0, 102, 153);
    started = false;
  }
}

public void updateText(double data)
{
    String output = "";
    
    if (isBetween(data, 0, 10))
    {
      output = "Obstacle at " + data + " is too close,  stopping buggy";
      text(output, 120, 120);
    }
    else if (isBetween(data, 10, 30)) 
    {
      output = "Obstacle detected at " + data + " cm, reducing speed.";
        text(output, 120, 120);
    }

}

public static boolean isBetween(double x, double lower, double upper) 
{
  return lower <= x && x <= upper;
}
