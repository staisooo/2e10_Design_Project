#include <PID_v2.h>

#include <WiFiNINA.h>

int status = WL_IDLE_STATUS;
char ssid[] = "Ermahgerd, wer fer";
char pass[] = "27122710";
WiFiServer server(5200);

//motor a connections 
const int enA = 9;
const int in1 = 8;
const int in2 = 7;
//motor b connections
const int enB = 3;
const int in3 = 4;
const int in4 = 5;

//Sensor Connection
const int LEYE = A1;
const int REYE = A0;

int left_sensor_state;
int right_sensor_state;

const int minSpeed = 100;
const int maxSpeed = 255;

long duration;

int trigPin = 11;
int echoPin = 10;

double kp = 6 ;
double ki = 0 ;
double kd = 1 ;

bool toMove = false;
bool toExitDistLoop = false;
bool toUpdate = true;

double distance, output, setPoint;
PID myPID(&distance, &output, &setPoint, kp, ki, kd, DIRECT);
int currentSpeed, previousSpeed;

double getDistance();

void setup(){
  Serial.begin(9600);
  distance = getDistance();
  setPoint = 30;
  currentSpeed = maxSpeed;
  previousSpeed = maxSpeed;
  myPID.SetMode(AUTOMATIC);

  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );
  
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  pinMode(trigPin, OUTPUT);         
  pinMode(echoPin, INPUT);  

  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);

  while (status != WL_CONNECTED) 
  {

    Serial.print("Attempting to connect to SSID: ");

    Serial.println(ssid);

    status = WiFi.begin(ssid, pass);
    Serial.print("Status: ");
    Serial.println(status);
    IPAddress localip = WiFi.localIP();
    IPAddress wifiIp = WiFi.gatewayIP();
    Serial.print("Local IP Address:");
    Serial.println(localip);
    Serial.print("Gateway IP Address:");
    Serial.println(wifiIp);
  }
  
  if (status == 3)
  {
    Serial.println("Connected to wifi");
  }

  Serial.println("Starting WiFi server");
  server.begin();
  Serial.print("Server status: ");
  Serial.println(server.status());

}

void loop()
{
  
  WiFiClient client = server.available();
  
  left_sensor_state = analogRead(LEYE);
  right_sensor_state = analogRead(REYE);
  
  distance = getDistance();
  myPID.Compute();
  server.write(distance);
  

  if (client.connected() ) 
  {
    Serial.println("Client Connected");
  }
  if (client.available())
  {
    Serial.println("Client is available");  
  }

  char c = client.read();
  
  if (c == 'g')
  {
    toMove = true;
  }

  if (c == 's')
  {
    toMove = false;
  }

  if(toMove == true)
  {
    toExitDistLoop = false;
    toUpdate = true;

    if(toUpdate == true)
    {
      server.write('c');
    }
 
    if(right_sensor_state > 500 && left_sensor_state < 500)
    {
      determiningSpeed();
      turn_right();
    }

    if(right_sensor_state < 500 && left_sensor_state > 500)
    {
      determiningSpeed();
      turn_left();
    }

    if(right_sensor_state < 500 && left_sensor_state < 500)
    {
      determiningSpeed();
      forwards();
    }

    if(right_sensor_state > 500 && left_sensor_state > 500)
    {
      brake_wheels();
    }


    if (distance <= 10)
    {
      setForwardSpeed(0);
    }
  }

  if(toMove == false) 
  {
    brake_wheels();
  }
}


double getDistance() {
  int echoTime;                  
  double calcualtedDistance;         
  
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW);

  echoTime = pulseIn(echoPin, HIGH);

  calcualtedDistance = echoTime / 58.26;  
  return calcualtedDistance;              
}


void setForwardSpeed(int buggySpeed)
{
  Serial.print("Setting speed to ");
  Serial.println(buggySpeed);
  //delay(2000);


  analogWrite(enA, buggySpeed);
  analogWrite(enB, buggySpeed);

  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
    
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void determiningSpeed()
{
 if(distance > setPoint)
 {
   setForwardSpeed(maxSpeed);
 }
 else if(distance >10 && distance <= setPoint)
 {
   Serial.print("Distance = ");
   Serial.println(distance);
   //delay(10);

   Serial.print("Output = ");
   Serial.println(output);
   Serial.print("SetPoint = ");
   Serial.println(setPoint);
   //delay(200);
   setForwardSpeed((maxSpeed - output));
  }
}

void brake_wheels()
{
  digitalWrite(in1, 0);
  digitalWrite(in2, 0);
  digitalWrite(in3, 0);
  digitalWrite(in4, 0);
}


void forwards()
{
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void backwards()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void turn_right()
{
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}

void turn_left()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

  
