/*
Some Notes:
Chose 5200 for both ports, I think 80 is a valid port number too, just
make sure the client and server are in the same port number. (and no other opened files you have are in that port number)

I had a problem with naming files so don't name your processing file just "Client" (I think it's a reserved keyword in Java that's why)

Commented out the setValue methods but you can put it back in for diagnostics to see if your buttons work.

Added a brakes button too.
*/

import controlP5.*;
import processing.net.*;

ControlP5 cp5;
Client myClient;
String data;

void setup() {
  size(400, 400);
  cp5 = new ControlP5(this);

  // possible things to change here: port number 
  // definitely change the IP address
  myClient = new Client(this, "192.168.1.20", 5200);  

  // Buttons 
  cp5.addButton("w")
    //.setValue(0)
    .setPosition(175, 100)
    .setSize(50, 50);

  cp5.addButton("a")
    //.setValue(0)
    .setPosition(125, 150)
    .setSize(50, 50);

  cp5.addButton("s")
    //.setValue(0)
    .setPosition(175, 150)
    .setSize(50, 50);

  cp5.addButton("d")
    //.setValue(0)
    .setPosition(225, 150)
    .setSize(50, 50);

  cp5.addButton("brakes")
    //.setValue(1)
    .setPosition(125, 201)
    .setSize(150, 30);
    
  
}

void draw() {
  
}

public void w(int theValue) {
  if (myClient.active()) {
    myClient.write("w");
    println("w key pressed");
  }
}
public void s(int theValue) {
  if (myClient.active()) {
    myClient.write("s");
    println("s key pressed");
  }
}
public void a(int theValue) {
  if (myClient.active()) {
    myClient.write("a");
    println("a key pressed");
  }
}
public void d(int theValue) {
  if (myClient.active()) {
    myClient.write("d");
    println("d key pressed");
  }
}
public void brakes(int theValue) {
  if (myClient.active()) {
    myClient.write("x");
    println("brakes key pressed");
  }
}
