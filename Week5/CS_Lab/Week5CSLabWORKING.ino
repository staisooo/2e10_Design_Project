
#include <WiFiNINA.h>


char ssid[] = "2E10NET";
char pass[] = "2E10Project";

WiFiServer server(5200);

//PWM controling pins
const int LMotor_PWM = A3;
const int RMotor_PWM = A2;

//Left Motor Connection
const int LMotor_FPIN = 21;
const int LMotor_BPIN = 20;

//Right Motor COnnection
const int RMotor_FPIN = 8;
const int RMotor_BPIN = 7;


void setup() {
  
  pinMode(LMotor_FPIN, OUTPUT);
  pinMode(LMotor_BPIN, OUTPUT);
  pinMode(RMotor_FPIN, OUTPUT);
  pinMode(RMotor_BPIN, OUTPUT);

  pinMode(RMotor_PWM, OUTPUT);
  pinMode(RMotor_PWM, OUTPUT);

  
  Serial.begin(9600);
  WiFi.beginAP(ssid, pass);
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address:");
  Serial.println(ip);
  server.begin();
}


void loop() {
  WiFiClient client = server.available();

  analogWrite(LMotor_PWM, 255);
  analogWrite(RMotor_PWM, 255);
  
  if (client.connected()) {
    Serial.println("Client Connected");
    char c = client.read();
    if (c == 'w') {
      Serial.println("Moving Robot Forwards");
      forwards();
    }
    if (c == 's') {
      Serial.println("Moving Robot Backwards");
      backwards();
    }
    if (c == 'a') {
      Serial.println("Moving Robot to the left");
      turn_left();
    }
    if (c == 'd') {
      Serial.println("Moving Robot to the right");
      turn_right();
    }
    if (c == 'x') {
      Serial.println("Stopping the Robot");
      brake_wheels();
    }
  }
}

void brake_wheels() {
  digitalWrite(RMotor_FPIN, LOW);
  digitalWrite(RMotor_BPIN, LOW);
    
  digitalWrite(LMotor_FPIN, LOW);
  digitalWrite(LMotor_BPIN, LOW);
}

void forwards() {
  digitalWrite(RMotor_FPIN, HIGH);
  digitalWrite(RMotor_BPIN, LOW);
    
  digitalWrite(LMotor_FPIN, HIGH);
  digitalWrite(LMotor_BPIN, LOW);
}

void backwards() {
  digitalWrite(RMotor_FPIN, LOW);
  digitalWrite(RMotor_BPIN, HIGH);
    
  digitalWrite(LMotor_FPIN, LOW);
  digitalWrite(LMotor_BPIN, HIGH);
}

void turn_right() {
  digitalWrite(RMotor_FPIN, LOW);
  digitalWrite(RMotor_BPIN, LOW);
    
  digitalWrite(LMotor_FPIN, HIGH);
  digitalWrite(LMotor_BPIN, LOW);;
}

void turn_left() {
  digitalWrite(RMotor_FPIN, HIGH);
  digitalWrite(RMotor_BPIN, LOW);
    
  digitalWrite(LMotor_FPIN, LOW);
  digitalWrite(LMotor_BPIN, LOW);

}
