/********************
  Not much to say here other than i left the pw section blank
 ********************/

#include <WiFiNINA.h>

char ssid[] = "vodafone-3DC1"; // mine had a hyphen
char pass[] = ""; // enter wifi pw
WiFiServer server(5200);

// Red moved my robot backwards and Black forwards
// Motor A connections 
const int RHS_Red = 6;
const int RHS_Black = 5;
// Motor B connections
const int LHS_Red = 2;
const int LHS_Black = 12;

const int HALF_SPEED = 128;

void setup() {
  Serial.begin(9600);

  pinMode(LHS_Red, OUTPUT);
  pinMode(LHS_Black, OUTPUT);
  pinMode(RHS_Red, OUTPUT);
  pinMode(RHS_Black, OUTPUT);
  
  WiFi.begin(ssid, pass);
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address:");
  Serial.println(ip);
  server.begin();
}

void loop() {
  WiFiClient client = server.available();

  if (client.connected()) {
    Serial.println("Client Connected");
    char c = client.read();
    if (c == 'w') {
      Serial.println("Moving Robot Forwards");
      forwards();
    }
    if (c == 's') {
      Serial.println("Moving Robot Backwards");
      backwards();
    }
    if (c == 'a') {
      Serial.println("Moving Robot to the left");
      turn_left();
    }
    if (c == 'd') {
      Serial.println("Moving Robot to the right");
      turn_right();
    }
    if (c == 'x') {
      Serial.println("Stopping the Robot");
      brake_wheels();
    }
  }
}

void brake_wheels() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, 0);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, 0);
}

void forwards() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, HALF_SPEED);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, HALF_SPEED);
}

void backwards() {
  analogWrite(LHS_Red, HALF_SPEED);
  analogWrite(LHS_Black, 0);
  analogWrite(RHS_Red, HALF_SPEED);
  analogWrite(RHS_Black, 0);
}

void turn_right() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, HALF_SPEED);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, 0);
}

void turn_left() {
  analogWrite(LHS_Red, 0);
  analogWrite(LHS_Black, 0);
  analogWrite(RHS_Red, 0);
  analogWrite(RHS_Black, HALF_SPEED);
}
