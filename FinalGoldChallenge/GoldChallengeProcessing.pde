import meter.*;
import controlP5.*;
import processing.net.*;
 
Meter m, m2, m3, m4;
ControlP5 cp5;
Client myClient;
Client myClient_aX;
Client myClient_aY;
Client myClient_gX;
Client myClient_gY;
PFont f;

int data;
boolean started = false;

void setup() {
 
  size(950, 700);
  background(255, 255, 200);
  cp5 = new ControlP5(this);
  myClient = new Client(this, "192.168.68.108", 9600);
  myClient_aX = new Client(this, "192.168.68.108", 80);
  myClient_aY = new Client(this, "192.168.68.108", 2400);
  myClient_gX = new Client(this, "192.168.68.108", 120);
  myClient_gY = new Client(this, "192.168.68.108", 5200);
  
  f = createFont("HoeflerText-Regular", 24);
  textFont(f);
    
  createMeters();
  
  cp5.addButton("go")
    //.setValue(0)
    .setPosition(250, 630)
    .setSize(200, 60);

  cp5.addButton("stop")
    //.setValue(0)
    .setPosition(470, 630)
    .setSize(200, 60);
  
}
void draw(){
  
  char ax, ay, gx, gy;
  
  if(myClient_aX.available() > 0)
  {
    ax = myClient_aX.readChar();
    int aX_int = int(ax);
    m.updateMeter(aX_int);
    print("aX ");
    println(aX_int);
    
  }
  
  if(myClient_aY.available() > 0)
  {
    ay = myClient_aY.readChar();
    int aY_int = int(ay);
    m2.updateMeter(aY_int);
    print("aY ");
    println(aY_int);
    
  }
  
  if(myClient_gX.available() > 0)
  {
    gx = myClient_gX.readChar();
    int gX_int = int(gx);
    m3.updateMeter(gX_int);
    print("gX ");
    println(gX_int);
    
  }
  
  if(myClient_gY.available() > 0)
  {
    gy = myClient_gY.readChar();
    int gY_int = int(gy);
    m4.updateMeter(gY_int);
    print("gY ");
    println(gY_int);
    
  }
}

public void go() 
{
  if (myClient.active()) 
  {
    myClient.write("g");
    noStroke();
    fill(255);
    rect(215, 580, 500, 30);
    fill(0, 102, 153);
    text("GO Key pressed. Starting the buggy.", 250, 600 );
    started = true;
  }
}

public void stop() 
{
  if (myClient.active()) 
  {
    myClient.write("s");
    noStroke();
    fill(255);
    rect(215, 580, 500, 30);
    fill(0, 102, 153);
    text("Stop key pressed. Stopping the buggy.", 250, 600 );
    started = false;
  }
}
 
void createMeters()
{
  m = new Meter(this, 10, 10);
  m.setMinScaleValue(0);
  m.setMaxScaleValue(200);        
  // Display digital meter value.
  m.setDisplayDigitalMeterValue(true);
  m.setTitle("X - Acceleration");
  m.setMinInputSignal(0);
  m.setMaxInputSignal(200);
  String[] scaleLabels = {"0", "20",  "40",  "60", "80", "100", "120", "140", "160", "180", "200"};
  m.setScaleLabels(scaleLabels);
  m.setShortTicsBetweenLongTics(9);
  
  m2 = new Meter(this, 470, 10);  
  m2.setMinScaleValue(0);
  m2.setMaxScaleValue(200);
  // Display digital meter value.
  m2.setDisplayDigitalMeterValue(true);
  m2.setTitle("Y - Acceleration");
  m2.setMinInputSignal(0);
  m2.setMaxInputSignal(200);
  m2.setScaleLabels(scaleLabels);
  m2.setShortTicsBetweenLongTics(9);
  
  m4 = new Meter(this, 10, 285);  
  m4.setMinScaleValue(0);
  m4.setMaxScaleValue(300);
  // Display digital meter value.
  m4.setDisplayDigitalMeterValue(true);
  m4.setTitle("X - Gyroscope");
  m4.setMinInputSignal(0);
  m4.setMaxInputSignal(300);
  String[] scaleLabels2 = {"0", "50", "100", "150", "200", "250", "300"};
  m4.setScaleLabels(scaleLabels2);
  m4.setShortTicsBetweenLongTics(9);
  
  m5 = new Meter(this, 470, 285);  
  m5.setMinScaleValue(0);
  m5.setMaxScaleValue(300);
  // Display digital meter value.
  m5.setDisplayDigitalMeterValue(true);
  m5.setTitle("Y - Gyroscope");
  m5.setMinInputSignal(0);
  m5.setMaxInputSignal(300);
  m5.setScaleLabels(scaleLabels2);
  m5.setShortTicsBetweenLongTics(9);
}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
